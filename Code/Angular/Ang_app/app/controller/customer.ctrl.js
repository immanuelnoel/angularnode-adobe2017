(function(){

	var ng = angular.module("customer_module", []);

	ng.controller("CustomerListController", function($scope){

		$scope.editMode = false;
		$scope.customers = [
		    { "id":1,
		      "firstName":"Rachel",
		      "lastName":"Green",
		      "gender":"female",
		      "address":"some address"
		    },
		    { "id":2,
		      "firstName":"Chandler",
		      "lastName":"Bing",
		      "gender":"male",
		      "address":"West Street"
		    },
		    { "id":3,
		      "firstName":"Joey",
		      "lastName":"Tribuanni",
		      "gender":"male",
		      "address":"some address"
		    },
		    { "id":4,
		      "firstName":"Monica",
		      "lastName":"Geller",
		      "gender":"female",
		      "address":"some address"
		    },
		    { "id":5,
		      "firstName":"Ross",
		      "lastName":"Geller",
		      "gender":"male",
		      "address":"some address"
		    }
		];


		$scope.deleteCustomer = function(id){

			$scope.customers.forEach(function(customer, index){

				if(customer.id == id){
					$scope.customers.splice(index, 1);
				}
			});
		};

		$scope.editCustomer = function(customer){

			$scope.currentCustomer = customer; 
			$scope.editMode = ($scope.editMode == true ? false : true);
		};

		$scope.updateCustomer = function(updatedCustomer){

			$scope.customers.forEach(function(customer, index){

				if(customer.id == updatedCustomer.id){
					
					$scope.customers[index] = updatedCustomer;
				
					$scope.editMode = false;
					$scope.currentCustomer = null;
				}
			});
		};
	});

})();