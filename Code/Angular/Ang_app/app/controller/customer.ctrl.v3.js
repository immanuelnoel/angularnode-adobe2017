(function(){

	var ng = angular.module("customer_module", ["service_module", "directive_module"]);

	ng.controller("CustomerMenuController", function($rootScope, $scope){

		$scope.searchText = "";

		$scope.filterCustomers = function(){

			$rootScope.$broadcast("filter_evt", $scope.searchText);
		};

	});


	ng.controller("CustomerListController", function($scope, CustomerService){

		$scope.editMode = false;
		$scope.customers = null;

		var customers = null;
		var orders = null;

		$scope.$on("filter_evt", function(evt, txt){

			var result = [];
			customers.forEach(function(c){

				if(c.firstName.toUpperCase().indexOf(txt.toUpperCase()) >= 0 ||
					c.lastName.toUpperCase().indexOf(txt.toUpperCase())  >= 0) {

					result.push(c);
				}
			});
			$scope.customers = result;
		});


		$scope.getCustomers = function(){

			CustomerService.getCustomers().then(function($data){

				$scope.customers = customers = $data; 
			});

		}();

		
		$scope.deleteCustomer = function(id){

			$scope.customers.forEach(function(customer, index){

				if(customer.id == id){
					$scope.customers.splice(index, 1);
					CustomerService.deleteCustomer(id);
				}
			});
		};

		$scope.editCustomer = function(customer){

			$scope.currentCustomer = customer; 
			$scope.editMode = true;
		};

		$scope.updateCustomer = function(updatedCustomer){

			CustomerService.updateCustomer($scope.currentCustomer.id, $scope.currentCustomer);
			$scope.editMode = false;
		};
	});

})();