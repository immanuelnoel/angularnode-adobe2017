
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/my_db');

var salesSchema = mongoose.Schema({

	category: String,
	product: String,
	sales: Number,
	quarter: Number
});

// Map collections to schema
sales = mongoose.model("sales", salesSchema);
sales.find({'sales':{'$gt':8000}}, function(err, docs){

	docs.forEach(function(sale){

		console.log(sale.category + " > " + sale.product + ": " + sale.sales);
	});
});


