
// 255, 0, 0 => ff0000
exports.rgbToHex = function(red, green, blue){

	var r = red.toString(16);
	var g = green.toString(16);
	var b = blue.toString(16);

	return padding(r) + padding(g) + padding(b);
};

// ff0000 => 255, 0, 0
exports.hexToRgb = function(hex){

	var red = parseInt(hex.substr(0, 2), 16);
	var green = parseInt(hex.substr(2, 4), 16);
	var blue = parseInt(hex.substr(4, 6), 16);

	return [red, green, blue];
};

function padding(hex){

	return (hex.length == 1 ? "0" + hex : hex);
}