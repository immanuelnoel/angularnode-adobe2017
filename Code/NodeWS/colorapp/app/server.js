var converter = require('./converter');

var http = require('http');
var url = require('url');

// http://localhost:8000/rgbtoHex?red=200&green=100&blue=13
// http://localhost:8000/hexToRgb?hex=ff0122

var server = http.createServer(function(request, response){

	response.writeHead(200, {"content-type":"text/plain"});

	var path = url.parse(request.url).pathname;
	var query= url.parse(request.url, true).query;

	switch(path.substring(1)){

		case 'rgbToHex':

			var result = converter.rgbToHex(parseInt(query.red),parseInt(query.green),parseInt(query.blue));
			response.end(result);
			break;

		case 'hexToRgb':
			
			var result = converter.hexToRgb(query.hex);
			response.end(JSON.stringify(result));
			break;

		default:
			response.end("Invalid method");
			break;
	}

});

server.listen(8000, function(){

	console.log("Listening on port 8000");
});