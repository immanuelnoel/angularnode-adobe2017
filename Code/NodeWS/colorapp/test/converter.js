
var converter = require('../app/converter');
var expect = require('chai').expect;

// Assemble your tests
describe('test', function(){

	//test spec
	it("test RGB to HEX", function(){

		var hex = converter.rgbToHex(255,0,0);
		expect(hex).to.equal('ff0000');

	});

	it("test HEX to RGB", function(){

		var rgb = converter.hexToRgb('ff0000');
		expect(rgb).to.be.deep.equal([255,0,0]);

	});

});

