(function(){

	var ng = angular.module("customer_module", ["service_module"]);

	ng.controller("CustomerListController", function($scope, CustomerService){

		$scope.editMode = false;
		$scope.customers = null;

		$scope.getCustomers = function(){

			var customers = null;
			CustomerService.getCustomers().then(function($data){

				$scope.customers = customers = $data; 
			});

		}();


		$scope.deleteCustomer = function(id){

			$scope.customers.forEach(function(customer, index){

				if(customer.id == id){
					$scope.customers.splice(index, 1);
					CustomerService.deleteCustomer(id);
				}
			});
		};

		$scope.editCustomer = function(customer){

			$scope.currentCustomer = customer; 
			$scope.editMode = ($scope.editMode == true ? false : true);
		};

		$scope.updateCustomer = function(updatedCustomer){

			CustomerService.updateCustomer($scope.currentCustomer.id, $scope.currentCustomer);
			$scope.editMode = false;
		};
	});

})();