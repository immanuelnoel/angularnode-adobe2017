(function(){

	var ng = angular.module("directive_module", []);

	ng.directive("cardView", function(){

		return {

			'restrict': 'E',
			'scope': {
				first: '=',
				second: '=',
				pic: '=',
				info: '=',
				delete: '&',
				update: '&',
			},
			templateUrl: 'app/template/card.tmpl.html'
		};

	});

})();