(function(){

	var ng = angular.module("service_module", []);

	ng.service("CustomerService", function($http,  $q){

		this.getCustomers = function(){

			var deferred = $q.defer();
			$http.get("http://localhost:3000/customers")
			.then(function(result){
				deferred.resolve(result.data);
			});	
			return deferred.promise; 
		};

		this.getOrders = function(){

			var deferred = $q.defer();
			$http.get("http://localhost:3000/sales")
			.then(function(result){
				deferred.resolve(result.data);
			});	
			return deferred.promise; 
		};

		this.deleteCustomer = function(id){

			$http.delete("http://localhost:3000/customers/" + id);
		};

		this.updateCustomer = function(id, customer){ 

			$http.put("http://localhost:3000/customers/" + id, customer);
		};

	});

})();