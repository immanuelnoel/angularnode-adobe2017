var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var customerSchema = mongoose.Schema({

	id: Number,
	firstName: String,
	lastName: String,
	gender: String
});

// Tie to collection
var customers = mongoose.model("customers", customerSchema);

// Get all customers
router.get('/', function(req, res, next) {
	customers.find({}, function(err, docs){
		console.log(err);
		res.json(docs);
	});
});

module.exports = router;
