var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var salesSchema = mongoose.Schema({

	category: String,
	product: String,
	sales: Number,
	quarter: Number
});

var sales = mongoose.model("sales", salesSchema);

router.get('/', function(req, res, next) {

	sales.find({'sales':{'$gt':3000}}, function(err, docs){

		res.json(docs);
	});
});

module.exports = router;
