var fs = require('fs');

var content = "";

var stream = fs.createReadStream('fs1.js');


stream.on('data', function(chunk){

	content += chunk;
	console.log("Chunk: " + chunk.toString());
});

stream.on('end', function(){

	console.log("Complete: " + content.toString());
});