var http = require('http');
var fs = require('fs');

var server = http.createServer(function(request, response){

	response.writeHead(200, {"content-type": "text/html"});
	
	var stream = fs.createReadStream('fs1.js');
	stream.on('data', function(chunk){

		response.write("Chunk: " + chunk.toString());
	});

	stream.on('end', function(){

		response.end("</br><h1>Done</h1>");
	});


});
server.listen(8000, function(){

	console.log("Server started [http://localhost:8000] ");
});